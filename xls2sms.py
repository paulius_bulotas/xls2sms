# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import sys
import json
import random
import string
import logging
import sqlite3
import datetime
from time import sleep
import pandas
from twilio.rest import Client
from twilio.base.exceptions import TwilioRestException


letsdoit = True
testers = ['Paulius Bulotas', 'Marius Bindokas']
creds_file = '.twilio.creds.json'
prefix = {
    'United States': '+1',
    'Lithuania': '+370',
    'Canada': '+1',
    'United Kingdom': '+44'
}


def logger(fileName):
    logging.basicConfig(
        format='%(asctime)s - %(levelname)s - %(message)s',
        level=logging.INFO,
        handlers=[
            logging.FileHandler(fileName),
            logging.StreamHandler()
        ])
    return logging


def fix_phone(number, location):
    if not len(number):
        raise
    if number == 'nan':
        raise
    number = number.replace(' ', '')
    number = number.replace('-', '')
    if number[0] != '+':
        if location == 'Lithuania' and number[0] == '8':
            number = prefix[location] + number[1:]
        elif location == 'Lithuania' and number.startswith('370'):
            number = '+' + number
        elif location == 'United Kingdom' and number[0] == '0':
            number = prefix[location] + number[1:]
        else:
            number = prefix[location] + number
    return number


def send_sms(to, email, password, location):
    tr = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(3))
    tr = ''
    s = 'Your AD password was synced with Office365 account. From now on use AD password for Outlook etc. Issues -> Slack #activesync'
    fromm = 'DEVBRIDGE'
    if location in ['United States', 'Canada']:
        fromm = '+17739851464'
    try:
        msg = client.messages.create(
            body=s,
            from_=fromm,
            to=to)
    except TwilioRestException as e:
        log.error('{}|{}|{}|{}'.format(to, email, str(e), tr))
        return None
    log.info('{}|{}|{}'.format(to, s.format(email=email,
                                            password='...',
                                            tracker=tr),
                               msg.sid))
    return msg.sid

if not len(sys.argv):
    print('Missing argument - excel file')
    sys.exit(1)

if not os.path.isfile(sys.argv[1]):
    print('{} does not exist'.format(sys.argv[1]))
    sys.exit(1)

dbname = os.path.splitext(sys.argv[1])[0]
log = logger(dbname + '.log')
twilio_log = logging.getLogger('twilio')
twilio_log.setLevel(logging.ERROR)
conn = sqlite3.connect('{}.db'.format(dbname))
c = conn.cursor()

try:
    c.execute('''SELECT count(*) FROM xls2sms''')
except Exception as e:
    c.execute('''CREATE TABLE xls2sms
                 (date text, email text, sent int)''')

client = None
# twilio.creds.json contains {"username": "...", "password": "..."}
try:
    with open(creds_file, 'r') as f:
        creds = json.load(f)
        client = Client(**creds)
except Exception:
    print('{} is missing'.format(creds_file))
    sys.exit(1)

xls = pandas.ExcelFile(sys.argv[1])
# print(xls.sheet_names)
parsed_xls = xls.parse()
for _, row in parsed_xls.iterrows():
    data = row[row.first_valid_index():]
    log.debug('keys: {}'.format(','.join(data.keys())))
    if any([data['First name Last name'] in testers, letsdoit]):
        r = c.execute("SELECT count(*) FROM xls2sms WHERE email=?",
                      (data['Work Email'],))
        s = r.fetchone()
        if s[0] == 0:
            phone = None
            try:
                phone = fix_phone(data['Mobile Phone'], data['Division'])
            except Exception:
                log.error('Invalid phone number {} for {}'.format(data['Mobile Phone'], data['First name Last name']))
                continue
            sms_result = send_sms(phone, data['Work Email'],
                                  '', data['Division'])
            if sms_result:
                c.execute("INSERT INTO xls2sms VALUES (?, ?, ?)",
                          (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                           data['Work Email'], 1))
                conn.commit()
            sleep(1)
        else:
            log.info('Skipping {}'.format(data['Work Email']))
